import React from 'react';
import { StackNavigator, SwitchNavigator, createStackNavigator } from 'react-navigation';

import RegistroScreen from './src/screens/RegistroScreen';
import LoginScreen from './src/screens/LoginScreen';
import RecuperarScreen from './src/screens/RecuperarScreen';
import SesionExitosaScreen from './src/screens/SesionExitosaScreen';
import RegistroExitosoScreen from './src/screens/RegistroExitosoScreen';
import RecuperarEnviadoScreen from './src/screens/RecuperarEnviadoScreen';
import HomeScreen from './src/screens/HomeScreen';
import ReporteScreen from './src/screens/ReporteScreen';
import PerfilScreen from './src/screens/PerfilScreen';
import ConfiguracionPerfilScreen from './src/screens/ConfiguracionPerfilScreen';

//const PantallasStack = StackNavigator({ Login: LoginScreen, Registro: RegistroScreen, Recuperacion: RecuperarScreen });

export default SwitchNavigator(
  {
    Login: LoginScreen,
    Registro: RegistroScreen,
    Recuperar: RecuperarScreen,
    SesionExitosa: SesionExitosaScreen,
    RegistroExitoso: RegistroExitosoScreen,
    RecuperarEnviado: RecuperarEnviadoScreen,
    Home: HomeScreen,
    Configuracion: ConfiguracionPerfilScreen,
    Perfil: PerfilScreen,
    Reporte: ReporteScreen
  },
  {
    initialRouteName: 'Login',
  }
);
