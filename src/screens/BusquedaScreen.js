import React from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import SearchBar from 'react-native-searchbar';

import SliderLive from '../components/SliderLive';
import PreviewVideos from '../components/PreviewVideos';

export default class BusquedaScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
      <TouchableOpacity
        style={styles.searchSection}
        onPress={() => this.searchBar.show()}>
        <Ionicons style={styles.searchIcon} name="ios-search" size={30} color="#E22A35"/>
      </TouchableOpacity>
                <SearchBar
          ref={(ref) => this.searchBar = ref}
          handleChangeText={()=>{}}
          onSubmitEditing={()=>{}}
          // data={items}
          // handleResults={this._handleResults}
          showOnLoad
          fontSize={20}
          backCloseSize={25}
          heightAdjust={-10}
          backgroundColor={'#E22A35'}
          iconColor={'#FAF7FA'}
          placeholderTextColor={'rgba(250,247,250,0.2)'}
          textColor={'#FAF7FA'}
        />
        <SliderLive/>
        <ScrollView>
        <PreviewVideos/>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:'column',
  },
  searchSection: {
    margin: 10,
  }
});