import React from 'react';
import { StackNavigator, SwitchNavigator, createStackNavigator } from 'react-navigation';

import PerfilScreen from '../screens/PerfilScreen';
import ConfiguracionPerfilScreen from '../screens/ConfiguracionPerfilScreen';

//const PantallasStack = StackNavigator({ Login: LoginScreen, Registro: RegistroScreen, Recuperacion: RecuperarScreen });

export default StackNavigator(
  {
    Configuracion: ConfiguracionPerfilScreen,
    Perfil: PerfilScreen,
  },
  {
    headerMode: 'none',
    initialRouteName: 'Perfil',
  }
);
