import React from 'react';
import { StackNavigator, SwitchNavigator, createStackNavigator } from 'react-navigation';

import TransmisionScreen from '../screens/TransmisionScreen';
import HomeScreen from './HomeScreen';
import TransmissionSeller from '../components/TransmissionComponents/TransmissionSeller';

//const PantallasStack = StackNavigator({ Login: LoginScreen, Registro: RegistroScreen, Recuperacion: RecuperarScreen });

export default StackNavigator(
  {
    TransmisionVendedor: TransmissionSeller,
    Transmision: TransmisionScreen,
  },
  {
    headerMode: 'none',
    initialRouteName: 'Transmision',
  }
);
