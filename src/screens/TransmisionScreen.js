import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { TabNavigator, TabBarBottom } from "react-navigation";

import Header from '../components/Header';
import FormTransmission from '../components/FormTransmission';

export default class TransmisionScreen extends React.Component {
  render() {
    const { containerTransmission } = styles;

    return (
      <View style={containerTransmission}>
        <Header title="Transmisión" />
        <FormTransmission />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerTransmission: {
    flex: 1,
  }
});