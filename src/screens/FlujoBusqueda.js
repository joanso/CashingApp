import React from 'react';
import { StackNavigator, SwitchNavigator, createStackNavigator } from 'react-navigation';

import TransmissionBuyer from '../components/TransmissionComponents/TransmissionBuyer';
import BusquedaScreen from './BusquedaScreen';
import ReporteScreen from './ReporteScreen';
import Buy from '../components/Buy';
import ReporteExitosoScreen from './ReporteExitosoScreen';
//const PantallasStack = StackNavigator({ Login: LoginScreen, Registro: RegistroScreen, Recuperacion: RecuperarScreen });

export default StackNavigator(
  {
    TransmisionComprador: TransmissionBuyer,
    Busqueda: BusquedaScreen,
    Reportar: ReporteScreen,
    Comprar: Buy,
    ReporteExitoso: ReporteExitosoScreen
  },
  {
    headerMode: 'none',
    initialRouteName: 'Busqueda',
  }
);
