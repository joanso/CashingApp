import React from 'react';
import { StyleSheet, Text, View, Image, KeyboardAvoidingView, ScrollView } from 'react-native';
import { withNavigation } from 'react-navigation';

import Header from '../components/Header';
import RegistroForm from '../components/RegistroForm';

class RegistroScreen extends React.Component {
  render() {
    return (
      <KeyboardAvoidingView behavior='padding' style={styles.container}>
        <ScrollView>
          <View style={styles.logoContainer}>
            <Image 
            style={styles.logo}
            source={require('../assets/imgs/cashing300.png')}
            />
            <Text style={styles.title}>Para tu registro necesitamos los siguientes datos</Text>
          </View>
          <View style={styles.formContainer}>
            <RegistroForm/>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

export default withNavigation(RegistroScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FAF7FA',
    paddingHorizontal: 10,
  },
  logoContainer: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center',
    paddingTop: 30
  },
  logo: {
    width: 100,
    height: 100
  },
  title:{
    color: '#E22A35',
    marginTop: 10,
    width: 200,
    textAlign: 'center',
    opacity: 0.8
  }
});