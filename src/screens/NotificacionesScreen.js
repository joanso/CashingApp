import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { TabNavigator, TabBarBottom } from "react-navigation";

import Header from '../components/Header';
import Chats from '../components/ChatsComponents/Chats';

export default class NotificacionesScreen extends React.Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <Header title="Notificaciones" />
        <Chats />
      </View>
    );
  }
}