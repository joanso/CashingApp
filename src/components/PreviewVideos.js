import React from 'react';
import { Dimensions, TouchableWithoutFeedback, StyleSheet, ScrollView, View, Image, Text } from 'react-native';
import { withNavigation } from 'react-navigation';
import { MaterialDialog } from 'react-native-material-dialog';
// import VideosList from './VideosList';

const width = Dimensions.get('window').width

class PreviewVideos extends React.Component{
	state = { 
		videos: [],
		imgPreview: 'empty',
		visible: false
	};

	componentWillMount(){
		fetch('https://rallycoding.herokuapp.com/api/music_albums')
	    	.then(response => response.json())
	    	.then(data => this.setState({ videos: data }));
	}

	renderVideos() {
		const { inputVideos } = styles;
		return this.state.videos.map( video => 
			<TouchableWithoutFeedback key={video.title}
				delayLongPress={700} 
				onPress={() => {this.props.navigation.navigate('TransmisionComprador')}}
				onLongPress={() => { this.setState({ visible: true }); this.setState({imgPreview: video.image}) }}
				onPressOut={() => { this.setState({ visible: false }) }}
			>
				<Image style={inputVideos} source={{ uri: video.image }} />
			</TouchableWithoutFeedback>
		);
	}

	render() {
		const { dialogContent, containerVideos, inputVideos, dialogVideo } = styles;

		return (
			<View style={styles.containerPreview}>
				<ScrollView>
					<View style={containerVideos}>
						{this.renderVideos()}
					</View>
				</ScrollView>
				<MaterialDialog
				  style={dialogContent}
				  title="Nombre de Usuario"
				  visible={this.state.visible}
				  onOk={() => { this.setState({ visible: false }) }}
				  onCancel={() => { this.setState({ visible: false }) }}
				  okLabel={""}
				  cancelLabel={""}
				>
				  <Image style={dialogVideo} source={{ uri: this.state.imgPreview }} />
				</MaterialDialog>
			</View>
		);
	}
}

export default withNavigation(PreviewVideos);

const styles = StyleSheet.create({
	containerPreview: {
		padding: 15,
	},
	containerVideos: {
		flexDirection: 'row',
		flexWrap: 'wrap'
	},
	inputVideos: { 
		width: (width / 2) - 50,
		height: 150,
		margin: 15,
	},
	dialogContent: {
		marginLeft: 12,
		marginRight: 12,
		borderRadius: 20,
	},
	dialogVideo: {
		width: width - 50,
		height: 250,
	}
});