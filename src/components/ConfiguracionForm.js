import React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { Zocial } from '@expo/vector-icons';
import { withNavigation } from 'react-navigation';

class ConfiguracionForm extends React.Component {
  
constructor(props) {
    super(props)
    this.state = {
      stripe: '',
      paypal: ''
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.containerIndicador}>
          <FontAwesome style={styles.icono} name="paypal" size={20} color="#d34836" />
          <Text style={styles.etiqueta}>Configurar cuenta PayPal</Text>
        </View>
        <TextInput 
          style={styles.input}
          placeholder='Correo asociado a PayPal'
          underlineColorAndroid='transparent'
          placeholderTextColor='rgba(173,81,38,0.4)'
          returnKeyType='next'
          onSubmitEditing={()=>this.passwordInput.focus()}
          keyboardType='email-address'
          autoCorrect={false}
          onChangeText={(value) => this.setState({paypal: value})}
        ></TextInput>
        <View style={styles.containerIndicador}>
          <Zocial style={styles.icono} name="stripe" size={20} color="#d34836" />
          <Text style={styles.etiqueta}>Configurar cuenta Stripe</Text>
        </View>
        <TextInput 
          style={styles.input}
          placeholder='Correo asociado a Stripe'
          underlineColorAndroid='transparent'
          placeholderTextColor='rgba(173,81,38,0.4)'
          returnKeyType='go'
          keyboardType='email-address'
          autoCorrect={false}
          ref={(input)=> this.passwordInput=input}
          onChangeText={(value) => this.setState({stripe: value})}
        ></TextInput>
        <View style={styles.buttomsContainer}>
          <TouchableOpacity 
            style={styles.buttomContainerGuardar}
            onPress={()=>{this.props.navigation.goBack()}}>
            <Text style={styles.buttomText}>Guargar</Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={styles.buttomContainerCancelar}
            onPress={()=>{this.props.navigation.goBack()}}>
            <Text style={styles.buttomText}>Cancelar</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default withNavigation(ConfiguracionForm);

const styles = StyleSheet.create({
  container: {
    padding: 10,
    justifyContent: 'center'
  },
  input: {
    borderRadius:15,
    height: 35,
    backgroundColor: 'rgba(173,81,38,0.2)',
    marginBottom: 10,
    color: 'rgba(173,81,38,0.9)',
    paddingHorizontal: 10
  },
  buttomContainer: {
    borderRadius:15,
    flex: 1,
    margin: 5,
    backgroundColor: '#fc5c65',
    paddingVertical: 12,
  },
  buttomText: {
    textAlign: 'center',
    color: '#FFFFFF',
    backgroundColor: 'transparent'
  },
  buttomsContainer: {
    flexDirection: 'row',
    marginTop: 30
  },
  buttomContainerGuardar: {
    borderRadius:15,
    flex: 1,
    marginTop: 5,
    marginRight: 5,
    marginBottom: 5,
    backgroundColor: 'rgba(217,145,65,0.9)',
    paddingVertical: 10,
  },
  buttomContainerCancelar: {
    borderRadius:15,
    flex: 1,
    marginTop: 5,
    marginRight: 5,
    marginBottom: 5,
    backgroundColor: 'rgba(217,145,65,0.7)',
    paddingVertical: 10,
  },
  containerIndicador: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  icono: {
    margin: 10
  },
  etiqueta: {
    color: '#AD5126',
  }
});