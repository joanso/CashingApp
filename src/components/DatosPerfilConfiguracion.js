import React from 'react';
import { StyleSheet, View, Image, Text, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { createStackNavigator } from 'react-navigation';
import { withNavigation } from 'react-navigation';

class DatosPerfilConfiguracion extends React.Component {
  render() {
    return (
      <View style={styles.containerUsuario}>
        <Image style={styles.avatarUsuario} source={ require('../assets/imgs/avatarPrueba.png') } />
        <View style={styles.containerDatos}>
          <Text style={styles.inputText} >Nombre de Usuario</Text>
          <Text style={styles.inputText} >Reputación</Text>
        </View>
				<View style={styles.containerConfiguracion}>
					<TouchableOpacity 
            style={styles.buttomContainer}
            onPress={()=>{this.props.navigation.navigate('Configuracion')}}>
            <Ionicons style={styles.icono} name="ios-cog" size={50} color="#E22A35" />
          </TouchableOpacity>
        </View>
		</View>

    );
  }
}

export default withNavigation(DatosPerfilConfiguracion);

const styles = StyleSheet.create({
	containerUsuario:{
		flexDirection: 'row',
		margin: 15
	},
	avatarUsuario: {
		flex: 1,
		height: 70,
		width: 70,
		borderRadius: 50,
		marginRight: 12,
		justifyContent: 'center',
	},
	containerDatos: {
		flex: 2,
		justifyContent: 'space-around',
		//backgroundColor: 'black'
	},
	containerConfiguracion: {
		flexDirection: 'row',
		flex: 1,
	},
	inputText: {
		fontSize: 17,
	},
	buttomContainer: {
		flexDirection: 'row',
    borderRadius:15,
    flex: 1,
    margin: 5,
		backgroundColor: 'transparent',
		justifyContent: 'flex-end',
		alignItems: 'center'
  },
});