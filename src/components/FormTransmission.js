import React from 'react';
import { StyleSheet, TextInput, View, TouchableOpacity, Text, Picker, ScrollView } from 'react-native';
import { withNavigation } from 'react-navigation';

import ModalSelector from 'react-native-modal-selector';

class FormTransmission extends React.Component {
	constructor(){
		super();
		this.state = {
			categoryValue: '',
			textInputValue: ''
		}
	};

  render() {

  	let index = 0;
    const data = [
        { key: index++, section: true, label: 'Categorías' },
        { key: index++, label: 'Categoría 1' },
        { key: index++, label: 'Categoría 2' },
        { key: index++, label: 'Categoría 3', accessibilityLabel: 'Tap here for cranberries' },
        // etc...
        // Can also add additional custom keys which are passed to the onChange callback
        { key: index++, label: 'Categoría 4', customKey: 'Not a fruit' }
    ];
  	
  	const { 
  		containerForm, 
  		textInput, 
  		marginTop20, 
  		containerButtons, 
  		button, 
  		buttonText, 
  		colorLight, 
  		colorDark, 
  		picker, 
  		textArea 
  	} = styles;

    return (
    	<ScrollView>
	    	<View style={containerForm}>
	    		<TextInput 
		          style={[textInput, marginTop20]}
		          placeholder='Nombre del producto'
		          underlineColorAndroid='transparent'
		          placeholderTextColor='rgba(226,42,53,4)'
		          autoCorrect={false}
		        ></TextInput>

	      		<ModalSelector
                    data={data}
                    initValue="Select something yummy!"
                    supportedOrientations={['portrait']}
                    accessible={true}
                    scrollViewAccessibilityLabel={'Scrollable options'}
                    cancelButtonAccessibilityLabel={'Cancel Button'}
                    onChange={(option)=>{ this.setState({textInputValue:option.label})}}>

                    <TextInput 
			          style={[textInput]}
			          placeholder='Selecciona una categoría'
			          underlineColorAndroid='transparent'
			          placeholderTextColor='rgba(226,42,53,0.4)'
			          autoCorrect={false}
			          editable={false}
			          value={this.state.textInputValue}
			        ></TextInput>

                </ModalSelector>

				<TextInput 
		          style={[textInput]}
		          placeholder='Cantidad'
		          underlineColorAndroid='transparent'
		          placeholderTextColor='rgba(226,42,53,0.4)'
		          autoCorrect={false}
		          keyboardType={'numeric'}
		        ></TextInput>

		        <TextInput 
		          style={[textInput]}
		          placeholder='Precio'
		          underlineColorAndroid='transparent'
		          placeholderTextColor='rgba(226,42,53,0.4)'
		          autoCorrect={false}
		          keyboardType={'numeric'}
		        ></TextInput>

	      		<TextInput 
		          style={[textInput, textArea]}
		          placeholder="Descripción (150 caracteres)"
		          underlineColorAndroid='transparent'
		          placeholderTextColor='rgba(226,42,53,0.4)'
		          autoCorrect={false}
		          multiline = {true} 
		          editable = {true} 
		          maxLength = {150} 
		          numberOfLines = {4}
		        ></TextInput>

	      		<View style={containerButtons}>
					<TouchableOpacity style={[button, colorLight]}
							onPress={()=>{this.props.navigation.navigate('TransmisionVendedor')}}>
			        	<Text style={buttonText}>Continuar</Text>
			    	</TouchableOpacity>
			    </View>
	      	</View>
	    </ScrollView>
    );
  }
}

export default withNavigation(FormTransmission);

const styles = StyleSheet.create({
	containerForm: {
		paddingLeft: 35,
		paddingRight: 35,
	},
	textInput: {
		borderRadius:15,
	    height: 35,
	    backgroundColor: 'rgba(226,42,53,0.2)',
	    marginBottom: 10,
	    color: 'rgba(226,42,53,0.9)',
	    paddingHorizontal: 10
	},
	textArea: {
		borderColor: '#E22A35',
		borderWidth: 1,
		height: 150,
		textAlignVertical: 'top'
	},
	picker: {
		marginBottom: 30,
	},
	marginTop20: {
		marginTop: 20
	},
	containerButtons: { 
    	flex: 1,
        flexDirection: 'row', 
        justifyContent: 'center', 
        marginBottom: 12, 
        paddingHorizontal: 5, 
        alignItems: 'flex-end'
    },
	button: {
		flex: 1,
		alignItems: 'center',
		padding: 10,
		borderRadius: 15
	},
	colorLight: {
		backgroundColor: '#E22A35',
		marginRight: 10
	},
	colorDark: {
		backgroundColor: '#E22A35',
	},
	buttonText: {
		color: 'white',
		fontWeight: 'bold'
	}
});