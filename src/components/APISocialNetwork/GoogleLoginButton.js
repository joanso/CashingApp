import React from 'react';
import { StyleSheet, Alert, Text, View, TextInput, TouchableOpacity, AsyncStorage } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { withNavigation } from 'react-navigation';
import Expo from 'expo';

//IDs para Google => CashingAppDevelopment => 6d:21:cb:e1:2a:0b:35:3b:2d:92:cf:ba:f7:20:4b:c5:ad:58:15:43
const idGoogleiOS = "86043144298-juup0gguo3kbitbfmjleskcvqgvr441j.apps.googleusercontent.com";
const idGoogleAndroid = "86043144298-beaks6cicgeq195kqpoh5134jgogu9e6.apps.googleusercontent.com";
const localStorage = '@MyStore:SD';

class GoogleLoginButton extends React.Component {

  render() {
    return (
        <TouchableOpacity 
            style={styles.buttomGoogleContainer}
            onPress={() => this.loginGoogle()}>
            <Ionicons style={styles.icono} name="logo-google" size={20} color="#d34836" />
            <Text style={styles.buttomTextGoogle}>Iniciar sesión con Google</Text>
        </TouchableOpacity>
    );
  }

  loginGoogle = async() => {
    try {
      const result = await Expo.Google.logInAsync({
      androidClientId: idGoogleAndroid,
      iosClientId: idGoogleiOS,
      scopes: ['profile', 'email'],
  });

  if(result.type === 'success'){
    fetch('http://10.0.0.107/cashingAppWebApi/public/api/user', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        name: result.user.name.toLowerCase(),
        email: result.user.email.toLowerCase(),
        picture: result.user.photoUrl,
      }),
    }).then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
        })
        .catch((error) => {
          console.log(error);
        });
        try{
          await AsyncStorage.setItem(localStorage, JSON.stringify({
            email: result.user.email.toLowerCase(),
            token: result.accessToken,
            status: 'true',
          }));
        }catch (error) {
          console.log('Error al guardar en el storage');
        }
        //console.warn(await AsyncStorage.getItem(localStorage));
        this.props.navigation.navigate('SesionExitosa');
      return result.accessToken;
  }else{
      return {cancelled: true};
  }
  }catch(e){
      return {error: true};
  }
  }
}

export default withNavigation(GoogleLoginButton);

const styles = StyleSheet.create({
  buttomsContainer: {
    flexDirection: 'row'
  },
  buttomGoogleContainer: {
    borderRadius:15,
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
    marginTop: 4,
    backgroundColor: 'transparent',
  },
  icono: {
    marginRight:5
  },
  buttomTextGoogle: {
    textAlign: 'center',
    color: '#d34836',
    backgroundColor: 'transparent'
  },
});