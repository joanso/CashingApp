import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native';
import { withNavigation } from 'react-navigation';
import { NavigationActions } from 'react-navigation'

const resetAction = NavigationActions.reset({
    index: 0,
    actions: [
      NavigationActions.navigate({ routeName: 'Home' })
    ]
  })

class Buy extends React.Component{
    
	render(){
        const { 
            containerText, 
            contentText, 
            containerImage, 
            contentImage, 
            imageDimenssion, 
            containerButtons, 
            button, 
            buttonText, 
            colorLight, 
            colorDark 
        } = styles;
		return(
            <View style={{flex: 1}}>
        <View style={containerText}>
        	<Text style={contentText} >Estás a punto de comprar nuestro producto, tienes 60 sg para concretar tu compra.</Text>
        </View>
        <View style={containerImage}>
        	<View style={contentImage}>
        		<Image source={require('../assets/imgs/card.png')} style={imageDimenssion} />
        		<Text style={{marginLeft: 8}}>Crédito</Text>
        	</View>
        	<View style={contentImage}>
        		<Image source={require('../assets/imgs/paypa.png')} style={imageDimenssion} />
        		<Text style={{marginLeft: 8}}>PayPal</Text>
        	</View>
        </View>
        <View style={containerButtons}>
        	<TouchableOpacity style={[button, colorLight]}
                onPress={()=>{this.props.navigation.goBack();this.props.navigation.dispatch(resetAction)}}>
	        	<Text style={buttonText}>Continuar</Text>
	    	</TouchableOpacity>

	    	<TouchableOpacity style={[button, colorDark]}
                onPress={()=>{this.props.navigation.goBack();this.props.navigation.dispatch(resetAction)}}>
	        	<Text style={buttonText}>Cancelar</Text>
	    	</TouchableOpacity>
        </View>
      </View>
		);
	}
}

export default withNavigation(Buy);

const styles = StyleSheet.create({
    containerText: { 
        flex: 1, 
        flexDirection: 'row', 
        'justifyContent': 'center' 
    },
    contentText: { 
        textAlign: 'center', 
        alignSelf: 'center', 
        fontSize: 20 
    },
    containerImage: { flex: 1, 
        flexDirection: 'column', 
        alignItems: 'center'
    },
    contentImage: {
        flexDirection: 'row', 
        alignItems: 'center'
    },
    imageDimenssion: {
        height: 70, 
        width: 70
    },
    containerButtons: { 
        flexDirection: 'row', 
        justifyContent: 'center', 
        marginBottom: 12, 
        paddingHorizontal: 5 
    },
	button: {
		flex: 1,
		alignItems: 'center',
		padding: 10,
		borderRadius: 15
	},
	colorLight: {
		backgroundColor: '#D99141',
		marginRight: 10
	},
	colorDark: {
		backgroundColor: '#B95B22',
	},
	buttonText: {
		color: 'white',
		fontWeight: 'bold'
	}
});