import React from 'react';
import { View, Image, Text, StyleSheet, Dimensions } from 'react-native';

export default class ContentFavorites extends React.Component {

	render(){

		const { containerFavorites, boxUser } = styles;

		return(
			<View style={containerFavorites}>
						
				<View style={boxUser}>
					<Image source={require('../imgs/avatar-default.png')} />
					<Text>Usuario</Text>
				</View>

				<View style={boxUser}>
					<Image source={require('../imgs/avatar-default.png')} />
					<Text>Usuario</Text>
				</View>

				<View style={boxUser}>
					<Image source={require('../imgs/avatar-default.png')} />
					<Text>Usuario</Text>
				</View>

				<View style={boxUser}>
					<Image source={require('../imgs/avatar-default.png')} />
					<Text>Usuario</Text>
				</View>

				<View style={boxUser}>
					<Image source={require('../imgs/avatar-default.png')} />
					<Text>Usuario</Text>
				</View>

				<View style={boxUser}>
					<Image source={require('../imgs/avatar-default.png')} />
					<Text>Usuario</Text>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	containerFavorites: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		flexWrap: 'wrap',
		paddingTop: 45,
		paddingBottom: 45,
	},
	boxUser: {
		flexDirection: 'column', 
		width: Dimensions.get('window').width / 2.2, 
		alignItems: 'center' ,
		marginBottom: 10
	}
});