import React from 'react';
import { Dimensions, TouchableWithoutFeedback, StyleSheet, FlatList, View, Image, Text } from 'react-native';

import DataUserProfile from './DataUserProfile';
import { MaterialDialog } from 'react-native-material-dialog';

const width = Dimensions.get('window').width;

export default class ProfileUser extends React.Component{

	state = { 
		videos: [],
		imgPreview: 'empty',
		visible: false
	};

	componentWillMount(){
		fetch('https://rallycoding.herokuapp.com/api/music_albums')
	    	.then(response => response.json())
	    	.then(data => this.setState({ videos: data }));
	}

	_keyExtractor = (item, index) => item.title;

	_renderVideos = ({item}) => {
		const { inputVideos } = styles;
		return(
			<TouchableWithoutFeedback style={{flexDirection: 'row'}} key={item.title}
				delayLongPress={700} 
				onLongPress={() => { this.setState({ visible: true }); this.setState({imgPreview: item.image}) }}
				onPressOut={() => { this.setState({ visible: false }) }}
			>
				<Image style={inputVideos} source={{ uri: item.image }} />
			</TouchableWithoutFeedback>
		);
	}

	render() {
		const { profileContainer, listVideosProfile, dialogContent, dialogVideo } = styles;
		return (
			<View style={profileContainer}>
				<DataUserProfile />
				<View style={listVideosProfile}>
					<FlatList
						data={this.state.videos}
						keyExtractor={this._keyExtractor}
						numColumns={3}
						style={{flexDirection: 'column'}}
						renderItem={this._renderVideos}
					>
					</FlatList>
					<MaterialDialog
					  style={dialogContent}
					  title="Nombre de Usuario"
					  visible={this.state.visible}
					  onOk={() => { this.setState({ visible: false }) }}
					  onCancel={() => { this.setState({ visible: false }) }}
					  okLabel={""}
					  cancelLabel={""}
					>
					  <Image style={dialogVideo} source={{ uri: this.state.imgPreview }} />
					</MaterialDialog>
				</View>
			</View>

		);
	}
}

const styles = StyleSheet.create({
	profileContainer: {
		flex: 1, 
		flexDirection: 'column',
	},
	listVideosProfile: {
		flex: 1,
		backgroundColor: '#FAF7FA'
	},
	inputVideos: { 
		width: (width / 3),
		height: (width / 3),
		margin: 1,
	},
	dialogContent: {
		marginLeft: 12,
		marginRight: 12,
		borderRadius: 20,
	},
	dialogVideo: {
		width: width - 50,
		height: 250,
	}
});