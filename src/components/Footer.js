import React from 'react';
import { StyleSheet, Text, View, StatusBar, TextInput } from 'react-native';
import { TabNavigator, TabBarBottom } from "react-navigation";
import { Ionicons } from '@expo/vector-icons';

import BusquedaScreen from '../screens/BusquedaScreen';
import FavoritosScreen from '../screens/FavoritosScreen';
import FlujoTransmision from '../screens/FlujoTransmision';
import NotificacionesScreen from '../screens/NotificacionesScreen';
import FlujoPerfil from '../screens/FlujoPerfil';
import FlujoBusqueda from '../screens/FlujoBusqueda';

StatusBar.setHidden(true);

const Footer = TabNavigator(
  {
    Busqueda: { screen: FlujoBusqueda },
    Favoritos: { screen: FavoritosScreen },
    Transmision: { screen: FlujoTransmision },
    Notificaciones: { screen: NotificacionesScreen },
    Perfil: { screen: FlujoPerfil }
  },
  {
    navigationOptions: ({ navigation }) => ({
      title: 'Prueba',
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if ( routeName == 'Busqueda' ){
          iconName = `ios-search${focused ? '' : '-outline'}`;
        } else if ( routeName == 'Favoritos' ){
          iconName = `ios-cart${focused ? '' : '-outline'}`;
        } else if ( routeName == 'Transmision' ){
          iconName = `ios-camera${focused ? '' : '-outline'}`;
        } else if ( routeName == 'Notificaciones' ){
          iconName = `ios-cash${focused ? '' : '-outline'}`;
        } else if ( routeName == 'Perfil' ){
          iconName = `ios-contact${focused ? '' : '-outline'}`;
        }
        return <Ionicons name={iconName} size={25} color="#E22A35" />
      }
    }),
    tabBarOptions: {
      showLabel:false,
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
      style: {
        backgroundColor: 'black'
      }
    },
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: false,
  }
);

export default Footer;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  }
});