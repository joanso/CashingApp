import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

import { Ionicons } from '@expo/vector-icons';
import ChatList from './ChatList';

const Chat = () => {
	const { headerChat, titleChat, chatboxesIcon, containerChat } = styles;

	return (
		<View style={containerChat}>
			<View style={headerChat}>
				<Text style={titleChat}>Chats</Text>
				<Ionicons style={chatboxesIcon} name="md-chatboxes" size={20} color="#000"/>
			</View>
			<ChatList />
		</View>
	);
};

const styles = StyleSheet.create({
	containerChat: {
		flex: 1,
	},
	headerChat: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		padding: 12
	},
	titleChat: {
		fontSize: 30,
		color: '#ddd'
	},
	chatboxesIcon: {
		fontSize: 35,
		color: '#B95B22'
	}
});

export default Chat;