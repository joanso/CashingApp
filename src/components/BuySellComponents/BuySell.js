import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

import BuySellList from './BuySellList';

const BuySell = () => {
	const { containerBuySell } = styles;

	return (
		<View style={containerBuySell}>
			<BuySellList />
		</View>
	);
};

const styles = StyleSheet.create({
	containerBuySell: {
		flex: 1,
	}
});

export default BuySell;