import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

//import Swiper from 'react-native-swiper';
import SwipeableParallaxCarousel from 'react-native-swipeable-parallax-carousel';

const dataReciente = [
  {
      "id": 339964,
      "title": "RECIENTE",
      "subtitle": "Computacion: Table ASUS",
      "imagePath": "https://i0.wp.com/pureinfotech.com/wp-content/uploads/2014/01/Asus-VivoTab-Note-8_large.jpg?w=780&quality=78&strip=all&ssl=1",
  },
  {
      "id": 315635,
      "title": "RECIENTE",
      "subtitle": "Tecnologia: Camara de seguridad MON",
      "imagePath": "http://o.aolcdn.com/hss/storage/midas/fd53e8982e159cd6e192dbe12b52a221/205959860/Moon-camera-780x439.jpg"
  },
  {
      "id": 339403,
      "title": "RECIENTE",
      "subtitle": "Coleccionables: Figuras de accion androides Star Wars",
      "imagePath": "http://i2.cdn.turner.com/money/dam/assets/170830125931-sphero-toys-780x439.jpg",
  },
];

export default class Slider extends React.Component {
  render() {
    return (
      <View style={styles.carousel}>
        <SwipeableParallaxCarousel 
          style={styles.carousel}
          height={180}
          titleColor='white'
          navigation={true}
          data={dataReciente}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:'column',
  },
  carousel: {
    marginTop: 15,
    marginRight: 15,
    marginLeft: 15
  }
});