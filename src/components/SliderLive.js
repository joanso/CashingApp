import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { withNavigation } from 'react-navigation';

//import Swiper from 'react-native-swiper';
import SwipeableParallaxCarousel from 'react-native-swipeable-parallax-carousel';

const dataLive = [
  {
      "id": 339964,
      "title": "LIVE",
      "subtitle": "DVD: Valerian and the City of a Thousand Planets",
      "imagePath": "https://image.tmdb.org/t/p/w780/o6OhxtsgMurL4h68Uqei0aSPMNr.jpg",
  },
  {
      "id": 315635,
      "title": "LIVE",
      "subtitle": "Blu-Ray: Spiderman Homecoming",
      "imagePath": "https://image.tmdb.org/t/p/w780/fn4n6uOYcB6Uh89nbNPoU2w80RV.jpg",
  },
  {
      "id": 339403,
      "title": "LIVE",
      "subtitle": "DVD: Baby Driver",
      "imagePath": "https://image.tmdb.org/t/p/w780/xWPXlLKSLGUNYzPqxDyhfij7bBi.jpg",
  },
];

class SlideLive extends React.Component {
  render() {
    return (
      <View style={styles.carousel}>
        <SwipeableParallaxCarousel 
          style={styles.carousel}
          height={180}
          titleColor='white'
          navigation={true}
          data={dataLive}
          onPress={() => {this.props.navigation.navigate('TransmisionComprador')}}
        />
      </View>
    );
  }
}

export default withNavigation(SlideLive);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:'column',
  },
  carousel: {
    marginTop: 15,
  }
});