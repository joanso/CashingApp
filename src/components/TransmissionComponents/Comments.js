import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

export default class Comment extends React.Component {
  render() {
    return (
      <View style={{ position: 'relative', borderRadius:15, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 4, backgroundColor: 'rgba(217,145,65,0.2)', marginHorizontal: 4, marginBottom: 3}}>
        <Image source={ require('../../imgs/avatar-default.png') } style={{ height: 50, width: 50, zIndex: 9 }}/>
        <Text style={{marginLeft: 8, zIndex: 9}}>Comentarios</Text>       
      </View>
    );
  }
}
