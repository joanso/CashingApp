import React from 'react';
import { StyleSheet, View, Image, Text } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { withNavigation } from 'react-navigation';

export default class DatosPerfil extends React.Component {
  render() {
    return (
      <View style={styles.containerUsuario}>
        <Image style={styles.avatarUsuario} source={ require('../assets/imgs/avatarPrueba.png') } />
        <View style={styles.containerDatos}>
          <Text style={styles.inputText} >Nombre de Usuario</Text>
          <Text style={styles.inputText} >Reputación</Text>
        </View>
		</View>

    );
  }
}

const styles = StyleSheet.create({
	containerUsuario:{
		flexDirection: 'row',
		margin: 15
	},
	avatarUsuario: {
		flex: 1,
		height: 70,
		width: 70,
		borderRadius: 50,
		marginRight: 12,
		justifyContent: 'center',
	},
	containerDatos: {
		flex: 2,
		justifyContent: 'space-around',
	},
	inputText: {
    fontSize: 17,
    color: '#d34836'
	},
});