import React from 'react';
import { StyleSheet, Alert, ActivityIndicator, View, Modal, TouchableOpacity, Text, TextInput } from 'react-native';
import { withNavigation } from 'react-navigation';

const url = 'http://10.0.0.107/cashingAppWebApi/public/api/auth/login';

class LoginRegisterButtons extends React.Component{

	constructor(props) {
	    super(props)
	    this.state = {
	      correo: '',
	      clave: '',
	      title: '',
	      message: '',
	      button: '',
	      loading: false,
	      disabled: true
	    }

	    this._userVerification = this._userVerification.bind(this);
	    this._alertVerification = this._alertVerification.bind(this);
	}

	/* Habilita o desahabilita boton de Iniciar Sesión */
	_longText = (value) => {
		if (value.length > 0) {
			this.setState({
				correo: value,
				disabled: false
			})
		}else{
			this.setState({
				disabled: true
			})
		}
	};

	/* Alert que despliega los distintos tipos de error: Credenciales erradas, 
	Verificación de correo, Conexión a la BD */
	_alertVerification = (resp) => {
		if (resp.auth == true) {
			this.setState({
		        loading: false
		    });
			{this.props.navigation.navigate('SesionExitosa')}
		}else{
		    if (resp.error == false) {
		      this.setState({
		        title: '¿Has verificado tu correo?',
		        message: 'Por favor verifica tu correo para validar la cuenta.',
		        button: 'Entendido',
		        loading: false
		      });
		    }else if(resp.error == 'Unauthorized'){
		      this.setState({
		        title: 'Error al introducir los datos',
		        message: 'Verifica tu correo o contraseña.',
		        button: 'Intentar de nuevo',
		        loading: false
		      });
		    }
		    Alert.alert(
		      this.state.title,
		      this.state.message,
		      [
		        {text: this.state.button, onPress: () => console.log('onpress')},
		      ],
		      { cancelable: false }
		    )
		}
	};

	/* Conexión con la api y verificación del usuario */
	async _userVerification(){
		this.setState({
	        loading: true
	    });

	    await fetch(url, {
	      method: 'POST',
	      headers: {
	        Accept: 'application/json',
	        'Content-Type': 'application/json',
	      },
	      body: JSON.stringify({
	        email: this.state.correo.toLowerCase(),
	        password: this.state.clave,
	      }),
	    }).then((response) => response.json())
	        .then(this._alertVerification)
	    .catch((error) => {
	      console.warn(error);
	    });
	}

	render(){
		return(
			<View>
				{/*Modal de loading*/}
			    {this.state.loading &&
			        <Modal
			          animationType="fade"
			          transparent={true}
			          visible={this.state.loading}
			          onRequestClose={ () => console.log('on request') }
			        >
			        	<View style={styles.modalDialog}>
				        	<ActivityIndicator 
				        		size="large" 
				        		color="#F44336" 
				        		animating={true}
				        		style={{alignSelf: 'center'}} 
				        	/>
				        </View>
			        </Modal>
			    }
				{/* Correo Electrónico */}
		        <TextInput 
		          style={styles.input}
		          placeholder='Correo electrónico'
		          underlineColorAndroid='transparent'
		          placeholderTextColor='rgba(226,42,53,0.4)'
		          returnKeyType='next'
		          onSubmitEditing={()=>this.passwordInput.focus()}
		          keyboardType='email-address'
		          autoCorrect={false}
		          onChangeText={this._longText}
		        ></TextInput>
		        {/* Contraseña */}
		        <TextInput 
		          style={styles.input}
		          placeholder='Contraseña'
		          underlineColorAndroid='transparent'
		          placeholderTextColor='rgba(226,42,53,0.4)'
		          returnKeyType='go'
		          secureTextEntry
		          ref={(input)=> this.passwordInput=input}
		          onChangeText={(value) => this.setState({clave: value})}
		        ></TextInput>
		        <View style={styles.buttomsContainer}>
		          {/*Inicio de sesión*/}
		          <TouchableOpacity 
		            style={styles.buttomContainerIniciar}
		            onPress={this._userVerification}
		            disabled={this.state.disabled}
		          >
		            <Text style={styles.buttomText}>Iniciar sesión</Text>
		          </TouchableOpacity>
		          {/*Registro de Usuarios*/}
		          <TouchableOpacity 
		            style={styles.buttomContainerRegistrar}
		            onPress={()=>{this.props.navigation.navigate('Registro')}}>
		            <Text style={styles.buttomText}>Registrarme</Text>
		          </TouchableOpacity>
		        </View>
		    </View>
		);
	}
}

export default withNavigation(LoginRegisterButtons);

const styles = StyleSheet.create({
  buttomText: {
    textAlign: 'center',
    color: '#FAF7FA',
    backgroundColor: 'transparent'
  },
  buttomsContainer: {
    flexDirection: 'row'
  },
  buttomContainerIniciar: {
    borderRadius:15,
    flex: 1,
    marginTop: 5,
    marginRight: 5,
    marginBottom: 5,
    backgroundColor: 'rgba(226,42,53,0.9)',
    paddingVertical: 10,
  },
  buttomContainerRegistrar: {
    borderRadius:15,
    flex: 1,
    marginTop: 5,
    marginRight: 5,
    marginBottom: 5,
    backgroundColor: 'rgba(226,42,53,0.7)',
    paddingVertical: 10,
  },
  input: {
    borderRadius:15,
    height: 35,
    backgroundColor: 'rgba(226,42,53,0.2)',
    marginBottom: 10,
    color: 'rgba(226,42,53,0.9)',
    paddingHorizontal: 10
  },
  modalDialog: {
  	flex: 1,
  	justifyContent: 'center',
  	backgroundColor: 'rgba(0,0,0,0.4)'
  }
});