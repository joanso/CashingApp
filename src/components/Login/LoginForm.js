import React from 'react';
import { StyleSheet, Alert, Text, View, TextInput, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { withNavigation } from 'react-navigation';

// Importando componentes
import LoginRegisterButtons from './LoginRegisterButtons';
import FacebookLoginButton from '../APISocialNetwork/FacebookLoginButton';
import GoogleLoginButton from '../APISocialNetwork/GoogleLoginButton';

class LoginForm extends React.Component {

  render() {
    return (
      <View style={styles.container}>
        {/* Contenedor de botónes */}
        <LoginRegisterButtons />
        {/*Facebook*/}
        <View style={styles.buttomsContainer}>
          <FacebookLoginButton/>
        </View>
        {/*Google*/}
        <View style={styles.buttomsContainer}>
          <GoogleLoginButton/>
        </View>
        {/*Recuperar contraseña*/}
        <View style={styles.buttomsContainer}>
          <TouchableOpacity 
            style={styles.buttomContainerOlvidar}
            onPress={()=>{this.props.navigation.navigate('Recuperar')}}>
            <Text style={styles.buttomTextOlvidar}>¿Has olvidado tu contraseña?</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default withNavigation(LoginForm);

const styles = StyleSheet.create({
  container: {
    padding: 10
  },
  input: {
    borderRadius:15,
    height: 35,
    backgroundColor: 'rgba(226,42,53,0.2)',
    marginBottom: 10,
    color: 'rgba(226,42,53,0.9)',
    paddingHorizontal: 10
  },
  buttomContainer: {
    borderRadius:15,
    flex: 1,
    margin: 5,
    backgroundColor: '#E22A35',
    paddingVertical: 12,
  },
  buttomText: {
    textAlign: 'center',
    color: '#FAF7FA',
    backgroundColor: 'transparent'
  },
  buttomsContainer: {
    flexDirection: 'row'
  },
  buttomContainerOlvidar: {
    borderRadius:15,
    flex: 1,
    margin: 5,
    backgroundColor: 'transparent',
    paddingVertical: 12,
  },
  buttomContainerIniciar: {
    borderRadius:15,
    flex: 1,
    marginTop: 5,
    marginRight: 5,
    marginBottom: 5,
    backgroundColor: 'rgba(226,42,53,0.9)',
    paddingVertical: 10,
  },
  buttomContainerRegistrar: {
    borderRadius:15,
    flex: 1,
    marginTop: 5,
    marginRight: 5,
    marginBottom: 5,
    backgroundColor: 'rgba(226,42,53,0.7)',
    paddingVertical: 10,
  },
  buttomFacebookContainer: {
    borderRadius:15,
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
    marginTop: 8,

    backgroundColor: 'transparent',
    paddingVertical: 5,
  },
  buttomGoogleContainer: {
    borderRadius:15,
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
    marginTop: 4,
    backgroundColor: 'transparent',
  },
  icono: {
    marginRight:5
  },
  buttomTextFacebook: {
    textAlign: 'center',
    color: '#3b5998',
    backgroundColor: 'transparent'
  },
  buttomTextGoogle: {
    textAlign: 'center',
    color: '#d34836',
    backgroundColor: 'transparent'
  },
  buttomContainerOlvidar: {
    borderRadius:15,
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
    marginTop: 8,
    backgroundColor: 'transparent',
  },
  buttomTextOlvidar: {
    textAlign: 'center',
    color: '#E22A35',
    backgroundColor: 'transparent',
    fontWeight: 'bold'
  }
});